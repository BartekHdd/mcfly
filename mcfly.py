#! /usr/bin/python
import requests
from bs4 import BeautifulSoup

url = "https://www.biletylotnicze.pl/airlines"
content = requests.get(url).content

soup = BeautifulSoup(content, 'html.parser')
flights = soup.find_all(class_='eds__content')

for flight in flights:
    departure = flight.find(class_='eds__departure').text
    arrival = flight.find(class_='eds__arrival').text
    price = flight.find(class_='price').text
    if int(price.split(" ")[0]) < 51:
        print(f"{departure} => {arrival}: \t {price}")
